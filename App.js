import React, { Component } from "react";
import {
    View,
    Text,
    Dimensions,
    Animated,
    PanResponder,
    ActivityIndicator
} from "react-native";
import NewsCard from './NewsCard';
import axios from 'axios';

const SCREEN_HEIGHT = Dimensions.get("window").height


class App extends Component {

    constructor(props) {
        super(props)

        this.position = new Animated.ValueXY()
        this.swipedCardPosition = new Animated.ValueXY({ x: 0, y: -SCREEN_HEIGHT })
        this.state = {
            currentIndex: 0,
            ARTICLES : [],
            isLoading : true
        }

    }

 

    renderMyData(){
        axios.get("http://newsapi.org/v2/top-headlines?country=in&category=general&apiKey=46cd85d6a8d447abb34a272c29a0a7fd")
            .then(response => 
                this.setState({ ARTICLES : response.data.articles, isLoading : false })
            ,error => 
                console.error(error)
            );
    }

    componentDidMount() {
        this.renderMyData();

        this.PanResponder = PanResponder.create({

            onStartShouldSetPanResponder: (e, gestureState) => true,
            onPanResponderMove: (evt, gestureState) => {

                if (gestureState.dy > 0 && (this.state.currentIndex > 0)) {
                    this.swipedCardPosition.setValue({
                        x: 0, y: -SCREEN_HEIGHT + gestureState.dy
                    })
                }
                else {
                    this.position.setValue({ x: 0, y: gestureState.dy }) 
                }
            },
            onPanResponderRelease: (evt, gestureState) => {

                if (this.state.currentIndex > 0 && gestureState.dy > 50 && gestureState.vy > 0.7) {
                    // console.log("IF :",this.state.ARTICLES.length, this.state.currentIndex)
                    Animated.timing(this.swipedCardPosition, {
                        toValue: ({ x: 0, y: 0 }),
                        duration: 400,
                        useNativeDriver: false
                    }).start(() => {

                        this.setState({ currentIndex: this.state.currentIndex - 1 })
                        this.swipedCardPosition.setValue({ x: 0, y: -SCREEN_HEIGHT })

                    })
                }
                else if (-gestureState.dy > 50 && -gestureState.vy > 0.7 && this.state.ARTICLES.length-1 > this.state.currentIndex) {
                    // console.log("ELIF :",this.state.ARTICLES.length, this.state.currentIndex)
                    Animated.timing(this.position, {
                        toValue: ({ x: 0, y: -SCREEN_HEIGHT }),
                        duration: 400,
                        useNativeDriver: false
                    }).start(() => {

                        this.setState({ currentIndex: this.state.currentIndex + 1 })
                        this.position.setValue({ x: 0, y: 0 })

                    })
                }
                else {
                    // console.log("ELSE :",this.state.ARTICLES.length, this.state.currentIndex)
                    Animated.parallel([
                        Animated.spring(this.position, {
                            toValue: ({ x: 0, y: 0 }),
                            useNativeDriver: false
                        }),
                        Animated.spring(this.swipedCardPosition, {
                            toValue: ({ x: 0, y: -SCREEN_HEIGHT }),
                            useNativeDriver: false
                        })

                    ]).start()

                }
            }
        })

    }

    renderArticles = () => {
        const News = this.state.ARTICLES;
        return News.length == 0 ? (<View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator color='#009387' size='large' animating={this.state.isLoading} />
        <Text>Please Wait...</Text>
    </View>) :

     News.map((item, i) => {

            if (i == this.state.currentIndex - 1) {

                return (
                    <Animated.View key={i} style={this.swipedCardPosition.getLayout()}
                        {...this.PanResponder.panHandlers}
                    >
                      <NewsCard data={item} />  
                    </Animated.View>
                )
            }
            else if (i < this.state.currentIndex) {
                return null
            }
            if (i == this.state.currentIndex) {

                return (

                    <Animated.View key={i} style={this.position.getLayout()}
                        {...this.PanResponder.panHandlers}
                    >
                        <NewsCard data={item} />  
                    </Animated.View>
                )
            }
            else {

                return (
                    <Animated.View key={i}
                    {...this.PanResponder.panHandlers}
                    >
                       <NewsCard data={item} />  
                    </Animated.View>
                )

            }
        }).reverse()

    }

    render() {
        return <View style={{flex : 1}}>
            {this.renderArticles()}
        </View>

    }
}
export default App;