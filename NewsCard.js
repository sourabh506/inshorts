import React from 'react';
import {
  Dimensions,
  View,
  Text,
  Image,
  Linking
} from 'react-native';

const NewsCard = (props) => {
  const SCREEN_HEIGHT = Dimensions.get('window').height;
  const SCREEN_WIDTH = Dimensions.get('window').width;

  return (
      <View 
        style={{
          flex: 1,
          position: 'absolute',
          height: SCREEN_HEIGHT,
          width: SCREEN_WIDTH,
          backgroundColor: 'white',
        }}>
        <View style={{flex: 2, backgroundColor: 'black'}}>
          <Image
            source={{uri: props.data.urlToImage}}
            style={{flex: 1, height: null, width: null, resizeMode: 'center'}}
          />
        </View>
        <View style={{flex: 3, marginHorizontal: 15}}>
          <Text numberOfLines={2} style={{fontWeight: 'bold', fontSize: 16}}>
            {props.data.title}
          </Text>
          <Text>
            {'\n'}
            {props.data.description}
          </Text>
        </View>
        <View style={{flex:0.8, backgroundColor:'#7f7f7f'}} >
          <Text style={{justifyContent:'center', textAlign:'center', fontSize:20, fontWeight:'bold', paddingVertical: 15, paddingHorizontal: 50}}  onPress={()=> Linking.openURL(props.data.url)} >Read More</Text>
        </View>
      </View>
  );
};

export default NewsCard;